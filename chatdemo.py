#!/usr/bin/env python
'''
    sxbbm message server , based on tornado framework.

    @Author: Lin-Yao Li
    @Date  : 2012-06-14
'''
import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid

from tornado.options import define, options

import pdb

define("port", default=8888, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/chatsocket", ChatSocketHandler),
        ]
        settings = dict(
            cookie_secret="43oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            autoescape=None,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        user = self.get_argument('name') 
        ChatSocketHandler.get_user(user)
        self.render("index.html", messages=ChatSocketHandler.cache)

class ChatSocketHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = []
    cache_size = 200

    # 1.user_bucket contains all user names;
    # 2.socket_bucket contains the correspondence 
    #   between user and object memory address.
    
    user_bucket = []
    socket_bucket = {}
   
    @classmethod 
    def get_user(cls, user):
        if not user in cls.user_bucket:
            cls.user_bucket.append(user)
        cls.user = user
        logging.info("User %r in bucket." % cls.user_bucket)

    def allow_draft76(self):
        # for iOS 5.0 Safari
        return True

    def open(self):
        logging.info('user <%r> open a new chat socket' % self.user)
        chat = {'id': str(uuid.uuid4()),
                'body': '%r has entered.' % self.user
            }
        chat['html'] = self.render_string('message.html', message=chat)
        for waiter in self.waiters:
            try:
                waiter.write_message(chat)
            except:
                logging.error('Error joining the chat.')

        self.socket_bucket[self] = self.user
        ChatSocketHandler.waiters.add(self)

    def on_close(self):
       # self.close()
        logging.info('user <%s> close a chat socket' % self.socket_bucket[self])
        chat =  {'id':str(uuid.uuid4()), 
                 'body': '%s has left.' % self.socket_bucket[self],
                }
        chat['html'] = self.render_string('message.html', message=chat)
        ChatSocketHandler.waiters.remove(self)

        self.user_bucket.remove(self.socket_bucket[self])
        del self.socket_bucket[self]

        for waiter in self.waiters:
            try:
                waiter.write_message(chat)
            except:
                logging.error('Error on closing socket.')
        #self.on_message('user <%s> has left.' % self.user)

    @classmethod
    def update_cache(cls, chat):
        '''
            Cache keeps the chat content temporally.
        '''
        cls.cache.append(chat)
        if len(cls.cache) > cls.cache_size:
            cls.cache = cls.cache[-cls.cache_size:]

    @classmethod
    def send_updates(cls, chat):
        '''
            This function actually write the messages at last.
        '''
        logging.info("sending message to %d waiters", len(cls.waiters))
        logging.info('Now we have users %r in bucket' % cls.user_bucket)
        for waiter in cls.waiters:
            try:
                waiter.write_message(chat)
            except:
                logging.error("Error sending message", exc_info=True)

    def on_message(self, message):
        '''
            override the original on_message(), emit one message to client.
        '''
        logging.info("got message %r", message)
        parsed = tornado.escape.json_decode(message)
        parsed['body'] = 'user < %s > said: %s ' % (self.user, parsed['body'])
        chat = {
            "id": str(uuid.uuid4()),
            "body": parsed["body"],
            }
        chat["html"] = self.render_string("message.html", message=chat)

        ChatSocketHandler.update_cache(chat)
        ChatSocketHandler.send_updates(chat)


def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
